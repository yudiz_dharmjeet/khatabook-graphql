import { Navigate, useRoutes } from 'react-router-dom';
// layouts
import DashboardLayout from './layouts/dashboard';
import LogoOnlyLayout from './layouts/LogoOnlyLayout';
//
// import Blog from './pages/Blog';
// import User from './pages/User';
import Login from './pages/Login';
import NotFound from './pages/Page404';
import Register from './pages/Register';
// import Products from './pages/Products';
import DashboardApp from './pages/DashboardApp';
import Khatabook from './pages/Khatabook';
import PrivateRoute from './routes/PrivateRoute';
import PublicRoute from './routes/PublicRoute';
import SingleKhatabook from './pages/SingleKhatabook';

// ----------------------------------------------------------------------

export default function Router() {
  return useRoutes([
    {
      path: '/dashboard',
      element: <DashboardLayout />,
      children: [
        { path: 'app', element: <PrivateRoute element=<DashboardApp /> /> },
        // { path: 'user', element: <PrivateRoute element=<User /> /> },
        // { path: 'products', element: <PrivateRoute element=<Products /> /> },
        // { path: 'blog', element: <PrivateRoute element=<Blog /> /> },
        { path: 'khatabook', element: <PrivateRoute element=<Khatabook /> /> },
        { path: 'khatabook/:id', element: <PrivateRoute element=<SingleKhatabook /> /> }
      ],
    },
    {
      path: 'login',
      element: <PublicRoute element=<Login /> />,
    },
    {
      path: 'register',
      element: <PublicRoute element=<Register /> />,
    },
    {
      path: '/',
      element: <LogoOnlyLayout />,
      children: [
        { path: '/', element: <Navigate to="/dashboard/app" /> },
        { path: '404', element: <NotFound /> },
        { path: '*', element: <Navigate to="/404" /> },
      ],
    },
    {
      path: '*',
      element: <Navigate to="/404" replace />,
    },
  ]);
}
