import PropTypes from 'prop-types';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
// material
import {  Card, Link, Typography, Stack } from '@mui/material';

// ----------------------------------------------------------------------

ShopProductCard.propTypes = {
  product: PropTypes.object,
};

export default function ShopProductCard({ product }) {
  const navigate = useNavigate()

  const { _id, sName, sBusinessName, sBusinessDetail } = product;

  function goToKhatabook() {
    navigate(`${_id}`)
  }

  return (
    <Card onClick={() => goToKhatabook()} sx={{ cursor: 'pointer' }}>

      <Stack spacing={2} sx={{ p: 3 }}>
        <Link to="#" color="inherit" underline="hover" component={RouterLink}>
          <Typography variant="subtitle2" noWrap>
            {sName}
          </Typography>
        </Link>

        <Typography variant="subtitle2" noWrap>
          {sBusinessName}
        </Typography>

        <Typography variant="subtitle2" noWrap>
          {sBusinessDetail}
        </Typography>

        
      </Stack>
    </Card>
  );
}
