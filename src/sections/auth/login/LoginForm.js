import { useEffect } from 'react';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
// import { useState } from 'react';
// import { useNavigate } from 'react-router-dom';
// form
import { useMutation } from '@apollo/client';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
// @mui
import { Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab';
// components
// import Iconify from '../../../components/Iconify';
import { LOGIN } from '../../../graphql/Auth/auth.mutation';
import { FormProvider, RHFTextField } from '../../../components/hook-form';

// ----------------------------------------------------------------------

LoginForm.propTypes = {
  setTab: PropTypes.func,
  setMobile: PropTypes.func
};

export default function LoginForm(props) {
  const { setTab, setMobile } = props
  // const [errMsg, setErrMsg] = useState('')

  // const [showPassword, setShowPassword] = useState(false);

  const [loginFunc, { data }] = useMutation(LOGIN);

  const LoginSchema = Yup.object().shape({
    mobile: Yup.string('Mobile must be a valid mobile').required('Mobile No. is required')
  });

  const defaultValues = {
    mobile: '',
    remember: true,
  };

  const methods = useForm({
    resolver: yupResolver(LoginSchema),
    defaultValues,
  });

  const {
    handleSubmit,
    formState: { isSubmitting },
  } = methods;

  const onSubmit = async (formdata) => {
    // navigate('/dashboard', { replace: true });
    setMobile(formdata.mobile)

    loginFunc({ variables: { login: {
      sMobileNo: formdata.mobile
    } } })
  };

  useEffect(() => {
    if (data) {
      setTab(2)
    }
  }, [data, setTab])

  return (
    <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
      <Stack spacing={3}>
        <RHFTextField name="mobile" label="Mobile No." />

        {/* <RHFTextField
          name="password"
          label="Password"
          type={showPassword ? 'text' : 'password'}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                  <Iconify icon={showPassword ? 'eva:eye-fill' : 'eva:eye-off-fill'} />
                </IconButton>
              </InputAdornment>
            ),
          }}
        /> */}
      </Stack>

      {/* <Stack direction="row" alignItems="center" justifyContent="space-between" sx={{ my: 2 }}>
        <RHFCheckbox name="remember" label="Remember me" />
        <Link variant="subtitle2" underline="hover">
          Forgot password?
        </Link>
      </Stack> */}

      <LoadingButton fullWidth size="large" type="submit" variant="contained" loading={isSubmitting} sx={{ my: 2 }}>
        Send Otp
      </LoadingButton>
    </FormProvider>
  );
}
