import { useEffect } from 'react';
import { useMutation } from '@apollo/client';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
// import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
// form
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
// @mui
import { Stack } from '@mui/material';
import { LoadingButton } from '@mui/lab';
// components
// import Iconify from '../../../components/Iconify';
import { FormProvider, RHFTextField } from '../../../components/hook-form';
import { VARIFY_LOGIN } from '../../../graphql/Auth/auth.mutation';

OtpForm.propTypes = {
  mobile: PropTypes.string,
  setTab: PropTypes.func
};

// ----------------------------------------------------------------------

export default function OtpForm(props) {
  const { mobile, setTab } = props

  const navigate = useNavigate();

  // const [showPassword, setShowPassword] = useState(false);

  const [varifyFunc, { data }] = useMutation(VARIFY_LOGIN);


  const LoginSchema = Yup.object().shape({
    otp: Yup.string('Otp must be a valid otp').required('Otp No. is required')
  });

  const defaultValues = {
    otp: '',
    remember: true,
  };

  const methods = useForm({
    resolver: yupResolver(LoginSchema),
    defaultValues,
  });

  const {
    handleSubmit,
    formState: { isSubmitting },
  } = methods;

  const onSubmit = async (formData) => {
    varifyFunc({
      variables: {
        verifylogin: {
          sOtp: formData.otp,
          sMobileNo: mobile
        }
      }
    })
  };

  useEffect(() => {
    if (data) {
      if (data.verifyLogin.sToken) {
        localStorage.setItem('token', data.verifyLogin.sToken)
        navigate('/dashboard/app');
        setTab(1)
      }
    }
  }, [data, setTab, navigate])

  return (
    <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
      <Stack spacing={3}>
        <RHFTextField name="otp" label="Otp No." />

        {/* <RHFTextField
          name="password"
          label="Password"
          type={showPassword ? 'text' : 'password'}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={() => setShowPassword(!showPassword)} edge="end">
                  <Iconify icon={showPassword ? 'eva:eye-fill' : 'eva:eye-off-fill'} />
                </IconButton>
              </InputAdornment>
            ),
          }}
        /> */}
      </Stack>

      {/* <Stack direction="row" alignItems="center" justifyContent="space-between" sx={{ my: 2 }}>
        <RHFCheckbox name="remember" label="Remember me" />
        <Link variant="subtitle2" underline="hover">
          Forgot password?
        </Link>
      </Stack> */}

      <LoadingButton fullWidth size="large" type="submit" variant="contained" loading={isSubmitting} sx={{ my: 2 }}>
        Login
      </LoadingButton>
    </FormProvider>
  );
}
