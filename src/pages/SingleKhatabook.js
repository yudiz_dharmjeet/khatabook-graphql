import { useNavigate, useParams } from 'react-router-dom';
// material
import { Button, Container, Typography } from '@mui/material';
// components
import { useQuery } from '@apollo/client';

import Page from '../components/Page';
// mock
import { GET_SINGLE_KHATABOOK } from '../graphql/Khatabook/khatabook.query';

// ----------------------------------------------------------------------

export default function SingleKhatabook() {

  const { id } = useParams ()

  const navigate = useNavigate()

  const { loading, data } = useQuery(GET_SINGLE_KHATABOOK, {
    variables: {getKhatabookByIdId: id}
  });

  function goBack() {
    navigate(-1)
  }

  if (loading) return 'Loading...';

  if (!data) return 'Error...'

  return (
    <Page title="Dashboard: Khatabook">
      <Container>
        <Typography variant="h4" sx={{ mb: 2 }}>
          Name : 
        </Typography>
        <Typography variant="h5" sx={{ mb: 5, ml: 5 }}>
          {data.getKhatabookById.sName}
        </Typography>
        <Typography variant="h4" sx={{ mb: 2 }}>
          Business Name : 
        </Typography>
        <Typography variant="h5" sx={{ mb: 5, ml: 5 }}>
          {data.getKhatabookById.sBusinessName}
        </Typography>
        <Typography variant="h4" sx={{ mb: 2 }}>
          Business Details : 
        </Typography>
        <Typography variant="h5" sx={{ mb: 5, ml: 5 }}>
          {data.getKhatabookById.sBusinessDetail}
        </Typography>
        <Button onClick={() => goBack()}>Back</Button>
      </Container>
    </Page>
  );
}
