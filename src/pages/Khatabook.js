import { useState } from 'react';
// material
import { Button, Container, FormControl, Input, InputLabel, Typography } from '@mui/material';
// components
import { useMutation, useQuery } from '@apollo/client';

import Page from '../components/Page';
import { ProductList } from '../sections/@dashboard/products';
// mock
import { GET_KHATABOOKS } from '../graphql/Khatabook/khatabook.query';
import { ADD_KHATABOOK } from '../graphql/Khatabook/khatabook.mutation';

// ----------------------------------------------------------------------

export default function Khatabook() {
  const [name, setName] = useState('')
  const [bName, setBName] = useState('')
  const [bDetails, setBDetails] = useState('')

  const { loading, data } = useQuery(GET_KHATABOOKS);

  const [addFunc, { data: addData }] = useMutation(ADD_KHATABOOK, {
    update(cache, addData) {
      const newKhatabook = addData.data.addKhatabook
      const data = cache.readQuery({ 
        query: GET_KHATABOOKS
      });
      cache.writeQuery({
        query: GET_KHATABOOKS,
        data: { getKhatabook: [...data.getKhatabook, newKhatabook] }
      })
    }
  });

  function AddKhatabook () {
    addFunc({
      variables: {
        khatabook : {
          sName: name,
          sBusinessName: bName,
          sBusinessDetail: bDetails
        }
      }
    })
  }

  console.log('addData', addData)

  if (loading) return 'Loading...';

  if (!data) return 'Error...'

  return (
    <Page title="Dashboard: Khatabook">
      <Container>
        <Typography variant="h4" sx={{ mb: 3 }}>
          Add Khatabook
        </Typography>
        <FormControl>
          <InputLabel htmlFor="name">Name</InputLabel>
          <Input id="name" aria-describedby="my-helper-text" onChange={(e) => setName(e.target.value)} />
        </FormControl>
        <FormControl sx={{ ml: 3 }}>
          <InputLabel htmlFor="bName">Business Name</InputLabel>
          <Input id="bName" aria-describedby="my-helper-text" onChange={(e) => setBName(e.target.value)} />
        </FormControl>
        <FormControl sx={{ ml: 3 }}>
          <InputLabel htmlFor="bDetails">Business Details</InputLabel>
          <Input id="bDetails" aria-describedby="my-helper-text" onChange={(e) => setBDetails(e.target.value)} />
        </FormControl>
        <Button sx={{ ml: 3 }} onClick={() => AddKhatabook()}>Add</Button>

        <Typography variant="h4" sx={{ mb: 5, mt: 5 }}>
          Khatabooks
        </Typography>
        <ProductList products={data.getKhatabook} />
      </Container>
    </Page>
  );
}
