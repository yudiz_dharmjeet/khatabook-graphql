import { ApolloProvider } from '@apollo/client';

// routes
import Router from './routes';
// theme
import ThemeProvider from './theme';
// components
import ScrollToTop from './components/ScrollToTop';
import { BaseOptionChartStyle } from './components/chart/BaseOptionChart';
import { client } from './graphql';

// css
import './App.css'

// ----------------------------------------------------------------------

export default function App() {
  return (
    <ApolloProvider client={client}>
      <ThemeProvider>
        <ScrollToTop />
        <BaseOptionChartStyle />
        <Router />
      </ThemeProvider>
    </ApolloProvider>
  );
}
