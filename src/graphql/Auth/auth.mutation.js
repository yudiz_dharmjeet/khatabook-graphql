import { gql } from '@apollo/client'

export const LOGIN = gql`
  mutation UserLogin($login: loginInput) {
    login(login: $login) {
      sMessage
    }
  }
`

export const VARIFY_LOGIN = gql`
  mutation VerifyUserLogin($verifylogin: verifyLogin) {
    verifyLogin(verifylogin: $verifylogin) {
      sMessage,
      sToken
    }
  }
`