import { gql } from '@apollo/client'

export const GET_KHATABOOKS = gql`
  query getKhatabooks {
    getKhatabook {
      _id
      sName
      sBusinessName
      sBusinessDetail
    }
  }
`

export const GET_SINGLE_KHATABOOK = gql`
  query getKhatabookById($getKhatabookByIdId: ID!) {
    getKhatabookById(id: $getKhatabookByIdId) {
      _id
      sName
      sBusinessName
      sBusinessDetail
    }
  }
`