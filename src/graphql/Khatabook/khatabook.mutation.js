import { gql } from '@apollo/client'

export const ADD_KHATABOOK = gql`
  mutation addKhatabook($khatabook: createKhatabook) {
    addKhatabook(khatabook: $khatabook) {
      _id
      sName
      sBusinessName
      sBusinessDetail
    }
  }
`